# Container creation

IMPORTANT: Before continuing we must have installed:
- docker: https://docs.docker.com/install/
- docker-compose: https://docs.docker.com/compose/install/

## Steps to follow

1. After Cloning the path repository: "https://gitlab.com/adga137/docker_containers.git".

2. Create the folder for the container and enter it

```
mkdir projectname
cd projectname
```

3. Copy all its contents from the lighttp-sqlite folder

```
cp -r ~/docker_containers/lighttp-sqlite/* ~/projectname/
```

4. The next step is to build the container and run it. To do this process, administrator permissions (root) are required in linux.

```
docker-compose up --build
```

5. Once the project is running, open an instance of the terminal or console as (root) in linux to access the container bash

```
docker exec -it projectname_php-fpm bash
```

6. Once inside the project bash we will be in the "~/src" folder if we need composer we can execute:

```
composer install
```

7. Once all the construction of the project is finished, each time we want to execute it, we only have to execute:

```
docker-compose up
```

8. Stop the container execution press keys in the execution console "Ctrl + c"

9. To access the container's web service, use the web browser through the following URL:

```
http://localhost:800
```

Note: to delete the image or container you must execute:

```
docker rmi projectname_php-fpm
docker rmi projectname_lighttp
```

Delete 
```
docker container rm idcontainer
```


To consult the list of container images execute:
```
docker images
```

To consult the list of containers execute:
```
docker ps -a
```

