# Creación del contenedor

IMPORTANTE: Antes de seguir deberemos tener instalado:
- docker: https://docs.docker.com/install/
- docker-compose: https://docs.docker.com/compose/install/

## Pasos a Seguir

1. Luego de Clonar del repositorio de la ruta: "https://gitlab.com/adga137/docker_containers.git".

2. Crear la carpeta del para el contenedor y entrar en ella

```
mkdir projectname
cd projectname
```

3. Copiar de la carpeta apache-sqlite todo su contenido

```
cp -r ~/docker_containers/apache-sqlite/* ~/projectname/
```

4. El siguiente paso es contruir el contenedor y ejecutarlo para hacer este proceso se requieren permisos de administrador (root) en linux

```
docker-compose up --build
```

5. Una vez en ejecución el proyecto abrir una instancia de la terminal o consola como (root) en linux para acceder al bash del contenedor

```
docker exec -it projectname_php-fpm bash
```

6. Una vez dentro de la bash del proyecto estaremos en la carpeta "~/src" si necesitamos composer podemos ejecutar:

```
composer install
```

7. Una vez terminadas toda la contrucción del proyecto cada vez que querramos ejecutarlo solo debemos ejecutar:

```
docker-compose up
```

8. Parar la ejecución del contenedor pulsar teclas en la consola de ejecución "Ctrl + c"

9. Para acceder al servicio web del contenedor utilizar el navegador web traves de la siguiente URL:

```
http://localhost:800
```

Nota: para eliminar la imagen o contenedor se debe ejecutar:

```
docker rmi projectname_php-fpm
docker rmi projectname_apache
```

Eliminar Contenedores
```
docker container rm idcontenedor
```

Para consultar la lista de imagenes de contenedores ejecutar:
```
docker images
```

Para consultar la lista de contenedores:
```
docker ps -a
```

