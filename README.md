
# English: Scripts project to build quick and easy docker containers | Spanish: Proyecto de scripts para construir contenedores docker fácil y rápido

- Apache + MariaDB + php-fpm (apache-mariadb)

- NGINX + MariaDB + php-fpm (nginx-mariadb)

- Lighttp + MariaDB + php-fpm (lighttp-mariadb)

- Apache + PostGreSQL + php-fpm (apache-postgresql)

- NGINX + PostGreSQL + php-fpm (nginx-postgresql)

- Lighttp + Postgresql + php-fpm (lighttp-postgresql)

- Apache + SQLite + php-fpm (apache-sqlite) new | nuevo

- NGINX + SQLite + php-fpm (nginx-sqlite) new | nuevo

- Lighttp + SQLite + php-fpm (lighttp-sqlite) new | nuevo

# English: Licence | Spanish: Licencia

- [GNU GPLv3](COPYING)
- [GNU GPLv3 url](https://www.gnu.org/licenses/gpl-3.0.html)

# English: Donate | Spanish: Donativos

Bitcoin (BTC) = 1GoxoTSkTHiVoBGtcoxW72tHc4qrVb9NEG

Litecoin (LTC) = LRqgbivRfHTqk3S2iU5QKN5Hsfnqufzikj

